#!/bin/bash

mkdir -p temp/
cd temp/ || exit 1

BASEDIR="$PWD"

mkdir -p deps/

# yaml-cpp
if [[ ! -f lib/libyaml-cpp.a ]] || [[ ! -d include/yaml-cpp ]]; then
	[[ -f lib/libyaml-cpp.a ]] && rm lib/libyaml-cpp.a
	[[ -d include/yaml-cpp ]] && rm -r include/yaml-cpp

	wget -nc https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-0.6.3.zip -O yaml-cpp.zip
	unzip -n yaml-cpp.zip -d yaml-cpp
	rm yaml-cpp.zip
	mv yaml-cpp/yaml-cpp-* yaml-cpp/sources

	pushd yaml-cpp || exit 1
	mkdir -p build/
	cmake -S sources/ -B build/ -DYAML_CPP_BUILD_TESTS=OFF
	cmake --build build/ -j
	cmake --install build --prefix "$BASEDIR/deps/"
	popd || exit 1
	rm -r yaml-cpp
fi