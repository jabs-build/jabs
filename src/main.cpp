#include "argument-parser.hpp"
#include "commands/command.hpp"
#include "project/module.hpp"
#include "project/project.hpp"
#include "application_flags.hpp"

#include <cstdlib>

#include <iostream>
#include <vector>

#include <string_view>
#include <string>

#include <memory>
#include <algorithm>
#include <stdexcept>

void print_usage(std::string_view command_name) {
	std::cerr << "Usage: " << command_name << " <command> [arguments...]\n";
	std::cerr << '\n';

	std::cerr << "Available commands are:\n";
	for (const auto& command : Command::get_all()) {
		std::cerr << '\t' << command->alias() << '\n';
	}
}

int run(std::string_view invocation,
		std::vector<std::string_view> argv) {
	ArgumentParser parser;
	Arguments args = parser.parse(argv);

	if (std::find(args.flags.begin(), args.flags.end(), "verbose") != args.flags.end()) {
		applicationFlags.verbose = true;
		std::cout << "Verbose printing enabled\n";
	}

	if (args.positionalArguments.empty()) {
		std::cerr << "ERROR: no command given\n\n";
		print_usage(invocation);
		return EXIT_FAILURE;
	}

	auto commandName = args.positionalArguments[0];
	auto command = Command::get_by_alias(commandName);
	if (!command) {
		std::cerr << "ERROR: command '" << commandName << "' not found\n";
		return EXIT_FAILURE;
	}

	std::unique_ptr<Project> project = {};
	if (command->requires_project()) {
		try {
			project = std::make_unique<Project>(".");
		} catch (std::runtime_error& e) {
			std::cerr << "ERROR: command '" << commandName
					  << "' requires a valid project to exist:\n";
			std::cerr << e.what() << std::endl;
			return EXIT_FAILURE;
		}
	}

	command->invoke(args, project.get());
	return EXIT_SUCCESS;
}

int main(int argc, const char* const* argv) {
	return run(argv[0], std::vector<std::string_view>(argv + 1, argv + argc));
}
