#pragma once

#include <string>
#include <vector>
#include <filesystem>

class SourceCollector {
public:
	std::vector<std::string> extensions;

	std::vector<std::filesystem::path> excluded_files;
	std::vector<std::filesystem::path> excluded_directories;
	std::vector<std::filesystem::path> additional_files;
	std::vector<std::filesystem::path> additional_directories;

	std::vector<std::filesystem::path> collect(const std::filesystem::path& directory) const;
	std::vector<std::filesystem::path>
	collect_all(const std::vector<std::filesystem::path>& directories) const;
};
