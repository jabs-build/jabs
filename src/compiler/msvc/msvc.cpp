#include "msvc.hpp"
#include "../../project/build_profile.hpp"

#include <stdexcept>

using namespace std::string_literals;

namespace fs = std::filesystem;

Invocation MSVCCompiler::build_compile_command_line(const fs::path& source,
												    const fs::path& target) const {
	std::vector<std::string> args;

	args.push_back("/c");
	args.push_back("/nologo");

	args.push_back("-std:"s + standard);

	args.push_back("/EHsc"); // TODO let user decide

	// TODO find best combinations of flags
	switch (buildProfile) {
	case BuildProfile::Debug:
		args.push_back("-Od");
		break;
	case BuildProfile::Release:
		args.push_back("-O2");
		break;
	}

	for (auto& includeDir : includeDirectories)
		args.push_back("-I"s + includeDir.string());

	args.push_back(source.string());

	args.push_back("-Fo"s + target.string());

	return {"cl", args};
}

Invocation
MSVCCompiler::build_link_command_line(const std::vector<fs::path>& objects,
								      const fs::path& output) const {
	std::vector<std::string> args;

	args.push_back("/nologo");

	args.push_back("/OUT:"s + output.string());

	for (const auto& pth : objects)
		args.push_back(pth.string());

	// TODO

	return {"link", args};
}

std::string MSVCCompiler::get_output_filename(const std::string& baseName) const {
	switch (outputType) {
	case OutputType::Executable:
		return baseName + ".exe"s;
	case OutputType::Shared:
		return baseName + ".dll"s;
	case OutputType::Static:
		return baseName + ".lib"s;
	}
	throw std::runtime_error("Invalid output type in getOutputFileName");
}
