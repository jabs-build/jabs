#pragma once

#include "../driver.hpp"
#include "../common.hpp"
#include <filesystem>
#include <string>
#include <vector>

namespace gcc { struct GCCOptions; }

class GNUCompiler : public CompilerDriver {
private:
	void populate_compile_options(gcc::GCCOptions& opts) const;

public:
	Invocation build_compile_command_line(const std::filesystem::path& source,
	                                      const std::filesystem::path& target) const override;
	CompileResult invoke_compile(const std::filesystem::path& source,
	                             const std::filesystem::path& target) const override;
	Invocation build_link_command_line(const std::vector<std::filesystem::path>& objects,
	                                   const std::filesystem::path& output) const override;
	std::string get_output_filename(const std::string& baseName) const override;
	virtual std::string get_config_hash() const override;
};
