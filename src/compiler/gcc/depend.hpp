#pragma once

#include <string>
#include <vector>

std::vector<std::string> parse_deps_file(const std::string& content);
