#pragma once

#include <map>
#include <string>
#include <vector>
#include <filesystem>
#include <optional>

#include "../common.hpp"

namespace gcc {

enum class OptimisationLevel {
	Default,

	Debug,
	Speed,
	Size,

	None,
	High,
	Higher,
	Highest
};

struct GCCOptions {
public:
	constexpr static const unsigned int OPTION_VERSION = 1;

	std::vector<std::filesystem::path> includeDirectories;
	std::vector<std::string> libraries;
	std::vector<std::filesystem::path> libraryDirectories;

	std::vector<std::string> defines;
	std::map<std::string, std::string> valueDefines;
	std::vector<std::string> undefines;

	std::optional<std::filesystem::path> dependencyFile;

	std::string standard = "c++17";
	OptimisationLevel optimisationLevel = OptimisationLevel::Default;
	bool debugSymbols = false;
	OutputType outputType = OutputType::Executable;
	bool pthread = false;
	bool lto = false;

	std::vector<std::string> extraFlags;

	Invocation compile_command(const std::filesystem::path& source,
							   const std::filesystem::path& target) const;
	Invocation link_command(const std::vector<std::filesystem::path>& objects,
						    const std::filesystem::path& output) const;
};

} // namespace gcc
