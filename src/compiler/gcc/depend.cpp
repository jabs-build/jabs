#include "depend.hpp"

using namespace std::string_literals;
#include <cctype>
#include <list>
#include <stdexcept>

bool is_path_character(char c) {
	return std::isalnum(c) || c == '/' || c == '.' || c == '_' || c == '-';
}

std::vector<std::string> parse_deps_file(const std::string& content) {
	std::vector<std::string> headerNames;

	// tokenize
	std::list<std::string> tokens;
	for (unsigned int i = 0; i < content.length();) {
		if (is_path_character(content[i])) {
			std::string path(1, content[i]);
			i++;
			for (; i < content.length() && is_path_character(content[i]); i++)
				path.push_back(content[i]);
			tokens.push_back(path);
		} else if (content[i] == ':') {
			tokens.push_back(std::string(1, ':'));
			i++;
		} else if (content[i] == '\\') {
			tokens.push_back(std::string(1, '\\'));
			i++;
		} else if (std::isspace(content[i])) {
			i++;
		} else {
			throw std::runtime_error("Unrecognized character '"s +
									 std::string(1, content[i]) + "'"s);
		}
	}

	// parse

	tokens.pop_front(); // remove output file
	if (tokens.front() != ":")
		throw std::runtime_error("Expected ':', got '"s + tokens.front() +
								 "' instead"s);
	tokens.pop_front();

	std::vector<std::string> headers;
	for (auto it = tokens.begin(); it != tokens.end(); it++) {
		const auto& str = *it;
		auto ppos = str.rfind('.');
		if (ppos != std::string::npos) {
			if (str.length() > ppos + 1 && str[ppos + 1] == 'h') {
				headers.push_back(str);
			}
		}
	}
	return headers;
}
