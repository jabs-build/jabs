#include "gcc_options.hpp"

using namespace std::string_literals;

namespace fs = std::filesystem;

namespace gcc {

constexpr const char* optimisation_level_to_string(OptimisationLevel level) {
	switch (level) {
	case OptimisationLevel::Default:
		return "0";
	case OptimisationLevel::Debug:
		return "g";
	case OptimisationLevel::Speed:
		return "fast";
	case OptimisationLevel::Size:
		return "s";
	case OptimisationLevel::High:
		return "1";
	case OptimisationLevel::Higher:
		return "2";
	case OptimisationLevel::Highest:
		return "3";
	default:
		return "0";
	}
}

Invocation GCCOptions::compile_command(const fs::path& source,
									   const fs::path& target) const {
	std::vector<std::string> args;

	args.push_back("-c");
	args.push_back("-std=" + standard);

	if (debugSymbols) {
		args.push_back("-g");
	}

	if (optimisationLevel != OptimisationLevel::Default)
		args.push_back("-O"s + optimisation_level_to_string(optimisationLevel));

	for (auto& includeDir : includeDirectories)
		args.push_back("-I"s + includeDir.string());

	args.push_back("-Wall");
	args.push_back("-Wextra");

	args.push_back(source.string());

	if (outputType == OutputType::Shared)
		args.push_back("-fPIC");

	args.push_back("-o");
	args.push_back(target.string());

	if (dependencyFile) {
		args.push_back("-MMD");
		args.push_back("-MF");
		args.push_back(dependencyFile->string());
	}

	args.insert(args.end(), extraFlags.begin(), extraFlags.end());

	return {"g++", args};
}

Invocation GCCOptions::link_command(const std::vector<fs::path>& objects,
								    const fs::path& output) const {
	std::vector<std::string> args;

	if (outputType != OutputType::Static) {
		args.push_back("-std="s + standard);

		if (optimisationLevel != OptimisationLevel::Default)
			args.push_back("-O"s +
						   optimisation_level_to_string(optimisationLevel));

		if (debugSymbols) {
			args.push_back("-g");
		}

		args.push_back("-o");
		args.push_back(output.string());

		if (outputType == OutputType::Shared) {
			args.push_back("-shared");
			args.push_back("-fPIC");
		}

		for (const auto& pth : objects)
			args.push_back(pth.string());

		for (auto& libDir : libraryDirectories)
			args.push_back("-L"s + libDir.string());

		for (const auto& library : libraries)
			args.push_back("-l"s + library);

		// if (dependencyFile) {
		// 	args.push_back("-MMD "s + dependencyFile->string());
		// }

		return {"g++", args};
	} else {
		args.push_back("-crs");
		args.push_back(output.string());

		for (const auto& pth : objects)
			args.push_back(pth.string());

		// TODO This probably has to be gcc-ar to support LTO?
		// (at least older binutils versions?)
		// see
		// https://gcc.gnu.org/wiki/LinkTimeOptimizationFAQ#ar.2C_nm_and_ranlib
		return {"ar", args};
	}
}

} // namespace gcc
