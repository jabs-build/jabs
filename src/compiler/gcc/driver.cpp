#include "driver.hpp"
#include "gcc_options.hpp"

#include "../../process.hpp"
#include "../../util/named_pipe.hpp"
#include "../../util/file.hpp"
#include "depend.hpp"

#include <fstream>
#include <thread>

#include <iostream>

#include <cstdio>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>

using namespace std::string_literals;

namespace fs = std::filesystem;

#define DRIVER_VERBOSE_FLAG false

void GNUCompiler::populate_compile_options(gcc::GCCOptions& options) const {
	// options.libraries = libraries;
	// options.libraryDirectories = libraryDirectories;
	options.includeDirectories = includeDirectories;
	options.standard = standard;
	options.outputType = outputType;
	options.extraFlags = additionalFlags;
	switch (buildProfile) {
	case BuildProfile::Debug: {
		options.debugSymbols = true;
		options.optimisationLevel = gcc::OptimisationLevel::Debug;
		options.defines.push_back(
			"_DEBUG"); // TODO these are placeholder defines
		break;
	}
	case BuildProfile::Release: {
		options.debugSymbols = false;
		options.optimisationLevel = gcc::OptimisationLevel::Highest;
		options.defines.push_back("NDEBUG");
		if (linkOptimisation)
			options.lto = true;
		break;
	}
	default:
		throw std::runtime_error("Build profile not known in GCC driver");
	}
}

Invocation GNUCompiler::build_compile_command_line(const fs::path& source,
												   const fs::path& target) const {
	gcc::GCCOptions options;
	populate_compile_options(options);
	return options.compile_command(source, target);
}

CompileResult GNUCompiler::invoke_compile(const fs::path& source,
										  const fs::path& target) const {
	gcc::GCCOptions options;
	populate_compile_options(options);

	const std::string extension = target.extension().string();
	fs::path depFile = target;
	depFile.replace_extension(extension.substr(0, extension.find_last_of('.')) +
							  ".d");
	options.dependencyFile = depFile;

	auto [executable, args] = options.compile_command(source, target);

	NamedPipe pipe(depFile, true);

	std::string depends;
	std::thread readerThread([&depFile, &depends]() {
		// TODO why not ifstream?
		PosixFileDescriptor fd(depFile, O_RDONLY);
		std::array<char, 1024> buf;
		while (true) {
			ssize_t size = read(fd.fd(), buf.data(), buf.size() - 1);
			if (size < 0) {
				std::string error(std::strerror(errno));
				throw std::runtime_error("Failed to read from file: "s + error);
			} else if (size == 0) {
				break;
			} else {
				depends.append(std::string(buf.data(), size));
			}
		}
	});

	if (DRIVER_VERBOSE_FLAG) {
		std::cout << executable << " ";
		for (const auto& arg : args) {
			std::cout << arg << " ";
		}
		std::cout << std::endl;
	}

	if (run_process(executable, args) != 0)
		throw std::runtime_error("Failed to compile");

	readerThread.join();
	pipe.close();

	const auto deps = parse_deps_file(depends);

	CompileResult result;
	result.includeDependencies.resize(deps.size());
	std::copy(deps.begin(), deps.end(), result.includeDependencies.begin());

	return result;
}

Invocation
GNUCompiler::build_link_command_line(const std::vector<fs::path>& objects,
								     const fs::path& output) const {
	gcc::GCCOptions options;
	options.libraries = libraries;
	options.libraryDirectories = libraryDirectories;
	// options.includeDirectories = includeDirectories;
	options.outputType = outputType;
	switch (buildProfile) {
	case BuildProfile::Debug: {
		options.debugSymbols = true;
		options.optimisationLevel = gcc::OptimisationLevel::Debug;
		// options.defines.push_back("_DEBUG"); // TODO these are placeholder
		// defines
		break;
	}
	case BuildProfile::Release: {
		options.debugSymbols = false;
		options.optimisationLevel = gcc::OptimisationLevel::Highest;
		// options.defines.push_back("NDEBUG");
		if (linkOptimisation)
			options.lto = true;
		break;
	}
	default:
		throw std::runtime_error("Build profile not known in GCC driver");
	}

	return options.link_command(objects, output);
}

std::string GNUCompiler::get_output_filename(const std::string& baseName) const {
	switch (outputType) {
	case OutputType::Executable:
		return baseName;
	case OutputType::Shared:
		return "lib"s + baseName + ".so"s;
	case OutputType::Static:
		return "lib"s + baseName + ".a"s;
	}
	throw std::runtime_error("Invalid output type in get_output_filename");
}

std::string GNUCompiler::get_config_hash() const {
	constexpr const unsigned int version = 1;
	return CompilerDriver::get_config_hash() + std::to_string(version);
}
