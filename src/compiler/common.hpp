#pragma once

#include <string>
#include <utility>
#include <vector>

using Invocation = std::pair<std::string, std::vector<std::string>>;

struct CompileResult {
	std::vector<std::string> includeDependencies;
	std::string output;
	// TODO error list

	CompileResult() = default;
};

enum class OutputType { Static, Shared, Executable };