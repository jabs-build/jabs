#pragma once

#include <filesystem>
#include <string>
#include <vector>

#include "../project/build_profile.hpp"
#include "../util/encoding.hpp"
#include "common.hpp"
using namespace encoding;

enum class OptimisationLevel { None, Debug, Speed, Size };

class CompilerDriver {
public:
	std::vector<std::string> libraries;
	std::vector<std::filesystem::path> includeDirectories;
	std::vector<std::filesystem::path> libraryDirectories;
	std::string standard = "c++17";
	BuildProfile buildProfile = BuildProfile::Debug;
	OutputType outputType = OutputType::Executable;
	bool linkOptimisation = false;
	std::vector<std::string> additionalFlags;

	virtual ~CompilerDriver() = default;

	virtual Invocation build_compile_command_line(const std::filesystem::path& source,
							                      const std::filesystem::path& target) const = 0;
	virtual CompileResult invoke_compile(const std::filesystem::path& source,
										 const std::filesystem::path& target) const;

	virtual Invocation build_link_command_line(const std::vector<std::filesystem::path>& objects,
											   const std::filesystem::path& output) const = 0;
	virtual CompileResult invoke_link(const std::vector<std::filesystem::path>& objects,
									 const std::filesystem::path& output) const;

	virtual std::string get_object_filename(const std::string& baseName) const;

	virtual std::string get_output_filename(const std::string& baseName) const;

	virtual std::string get_config_hash() const;
};
