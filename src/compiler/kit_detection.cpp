#include "kit_detection.hpp"

#include <cstdlib>
#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <limits.h>
#include <unistd.h>

#include <filesystem>
namespace fs = std::filesystem;

constexpr char PATH_SEPARATOR =
#ifdef _WIN32
	';';
#else
	':';
#endif

std::vector<std::string> split(const std::string& value, char delimiter) {
	std::vector<std::string> output;
	size_t pos = 0, last_pos = 0;
	while ((pos = value.find(delimiter, last_pos)) != std::string::npos) {
		output.push_back(value.substr(last_pos, pos - last_pos));
		last_pos = pos + 1;
	}
	return output;
}

fs::path get_symlink_target(const fs::path& link) {
	char buff[PATH_MAX];
	ssize_t len = readlink(link.c_str(), buff, sizeof(buff) - 1);
	if (len == -1)
		throw std::runtime_error("readlink failed");
	buff[len] = '\0';
	std::string link_name(buff);
	return fs::canonical(link.parent_path() / fs::path(link_name));
}

std::vector<fs::path> get_valid_path_entries() {
	std::string path = std::getenv("PATH");
	auto path_entries = split(path, PATH_SEPARATOR);
	std::vector<fs::path> entry_paths;
	for (const auto& entry : path_entries) {
		fs::path entry_path(entry);
		if (fs::exists(entry_path))
			entry_paths.push_back(entry_path);
	}
	return entry_paths;
}

void find_kits() {
	auto path_entries = get_valid_path_entries();
	const std::regex gcc_regex(
		"^((\\w+-)*)gcc(-\\d+(\\.\\d+(\\.\\d+)?)?)?(\\.exe)?$",
		std::regex_constants::ECMAScript);
	const std::regex clang_regex("^clang(-\\d+(\\.\\d+(\\.\\d+)?)?)?(\\.exe)?$",
								 std::regex_constants::ECMAScript);

	for (const auto& entry : path_entries) {
		for (const auto& file : fs::directory_iterator(entry)) {
			if (file.is_regular_file() || file.is_symlink()) {
				std::smatch m;
				std::string filename(file.path().filename().string());
				if (std::regex_match(filename, m, gcc_regex)) {
					std::cout << "gcc: \"" << file.path().string() << "\";"
							  << std::endl;
				} else if (std::regex_match(filename, m, clang_regex)) {
					std::cout << "clang: \"" << file.path().string() << "\";"
							  << std::endl;
				}
			}
		}
	}
}
