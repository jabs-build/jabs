#include "driver.hpp"
#include "../process.hpp"

#include <sstream>
#include <stdexcept>

using namespace std::string_literals;

CompileResult CompilerDriver::invoke_compile(const std::filesystem::path& source,
											const std::filesystem::path& target) const {
	auto [executable, args] = build_compile_command_line(source, target);
	if (run_process(executable, args) != 0)
		throw std::runtime_error("Failed to compile file "s + source.string());
	return CompileResult();
}

CompileResult CompilerDriver::invoke_link(const std::vector<std::filesystem::path>& objects,
										 const std::filesystem::path& output) const {
	auto [executable, args] = build_link_command_line(objects, output);
	if (run_process(executable, args) != 0)
		throw std::runtime_error("Failed to link");
	return CompileResult();
}

std::string
CompilerDriver::get_object_filename(const std::string& baseName) const {
	return baseName + ".o"s;
}

std::string
CompilerDriver::get_output_filename(const std::string& baseName) const {
	return baseName;
}

std::string CompilerDriver::get_config_hash() const {
	std::stringstream ss;
	for (auto& lib : libraries)
		ss << lib << ',';
	ss << ';';
	for (auto& libdir : libraryDirectories)
		ss << libdir.string() << ',';
	ss << ';';
	for (auto& incdir : includeDirectories)
		ss << incdir.string() << ',';
	ss << ';';
	ss << standard << ';';
	ss << static_cast<int>(buildProfile) << ';';
	ss << static_cast<int>(outputType) << ';';
	ss << (linkOptimisation ? '1' : '0') << ';';
	return hex::encode_int(std::hash<std::string>()(ss.str()));
}
