#include "registry.hpp"
#include "../util/encoding.hpp"
#include "net.hpp"
using namespace encoding;

#include <fstream>
#include <vector>
#include <string_view>

using namespace std::string_literals;

namespace fs = std::filesystem;

std::string build_url(const std::string_view base,
					  const std::string_view path) {
	std::string result;
	result.reserve(base.length() + path.length() + 1);
	result.append(base);
	if (base.size() > 0 && path.size() > 0) {
		if (base.at(base.size() - 1) != '/' && path.at(0) != '/')
			result.append(1, '/');
		result.append(path);
	}
	return result;
}

void Registry::fetch_package(const std::string& packageName,
							 const std::string& version,
							 const fs::path& targetPath) const {
	net::fetch(build_url(m_registryURL, packageName + "/"s + version + "/tgz"s),
			   targetPath);
}

void Registry::publish_package(const std::string& packageName,
							   const std::string& version,
							   const fs::path& sourcePath) const {
	std::ifstream input(sourcePath,
						std::ios::in | std::ios::binary | std::ios::ate);
	auto size = input.tellg();
	input.seekg(0, std::ios::beg);

	std::vector<char> data(size);
	input.read(data.data(), size);

	std::stringstream ss;
	ss << "{\"data\": \"" << base64::encode(data.begin(), data.end()) << "\"}";
	net::push_string(build_url(m_registryURL, packageName + "/"s + version),
					 ss.str());
}
