#include "net.hpp"

#include <cstdio>
#include <cstring>
#include <curl/curl.h>
#include <stdexcept>
#include <algorithm>
#include "../util/file.hpp"

using namespace std::string_literals;
namespace fs = std::filesystem;

namespace net {

class CURLGlobalState {
public:
	CURLGlobalState() { curl_global_init(CURL_GLOBAL_ALL); }
	~CURLGlobalState() { curl_global_cleanup(); }

	CURLGlobalState(const CURLGlobalState&) = delete;
	CURLGlobalState& operator=(const CURLGlobalState&) = delete;
};

static CURLGlobalState curl_global_state;

class CurlHandle {
private:
	CURL* curl;

public:
	CurlHandle() : curl(curl_easy_init()) {
		if (!curl)
			throw std::runtime_error("Failed to create curl handle");
	}

	~CurlHandle() { curl_easy_cleanup(curl); }

	CURL* handle() const { return curl; }

	template <typename T> CURLcode option(CURLoption option, T arg) {
		return curl_easy_setopt(curl, option, arg);
	}

	CURLcode perform() { return curl_easy_perform(curl); }
};

static size_t write_data(void* ptr, size_t size, size_t nmemb, FILE* stream) {
	return std::fwrite(ptr, size, nmemb, stream);
}

void fetch(const std::string& url, const fs::path& path, bool fail_on_error) {
	CurlHandle handle;
	handle.option(CURLOPT_URL, url.c_str());
	handle.option(CURLOPT_WRITEFUNCTION, write_data);
	if (fail_on_error)
		handle.option(CURLOPT_FAILONERROR, 1L);

	CFile file(path, "wb");
	handle.option(CURLOPT_WRITEDATA, file.file());

	auto result = handle.perform();
	if (result != CURLE_OK)
		throw std::runtime_error("Request failed: "s +
								 std::string(curl_easy_strerror(result)));
}

void push(const std::string& url, const fs::path& path) {
	CurlHandle handle;
	handle.option(CURLOPT_URL, url.c_str());
	handle.option(CURLOPT_UPLOAD, 1L);
	handle.option(CURLOPT_FAILONERROR, 1L);
	handle.option(CURLOPT_CUSTOMREQUEST, "PUT");

	CFile file(path, "rb");
	// TODO this seems to require opt READFUNCTION on Windows
	// according to https://curl.haxx.se/libcurl/c/fileupload.html
	handle.option(CURLOPT_READDATA, file.file());

	auto result = handle.perform();
	if (result != CURLE_OK)
		throw std::runtime_error("Request failed: "s +
								 std::string(curl_easy_strerror(result)));
}

struct strptr {
private:
	const char* data;
	size_t length;
	size_t read_offset;

public:
	strptr(const char* _data, size_t _length)
		: data(_data), length(_length), read_offset(0) {}

	inline size_t read(char* buffer, size_t size) {
		size_t remaining = length - read_offset;
		size_t copy = std::min(remaining, size);
		memcpy(buffer, data + read_offset, copy);
		read_offset += copy;
		return copy;
	}
};

size_t read_callback(char* buffer, size_t size, size_t nitems, void* userdata) {
	strptr* data = static_cast<strptr*>(userdata);
	return data->read(buffer, size * nitems);
}

void push_string(const std::string& url, const std::string& data,
				 const std::string& content_type) {
	CurlHandle handle;
	handle.option(CURLOPT_URL, url.c_str());
	handle.option(CURLOPT_UPLOAD, 1L);
	handle.option(CURLOPT_FAILONERROR, 1L);
	handle.option(CURLOPT_CUSTOMREQUEST, "PUT");

	curl_slist* headers = nullptr;
	headers =
		curl_slist_append(headers, ("Content-Type: "s + content_type).c_str());
	handle.option(CURLOPT_HTTPHEADER, headers);

	strptr ptr(data.data(), data.length());
	handle.option(CURLOPT_READFUNCTION, read_callback);
	handle.option(CURLOPT_READDATA, &ptr);

	auto result = handle.perform();
	curl_slist_free_all(headers);
	if (result != CURLE_OK)
		throw std::runtime_error("Request failed: "s +
								 std::string(curl_easy_strerror(result)));
}

} // namespace net
