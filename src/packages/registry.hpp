#pragma once

#include <string>
#include <filesystem>

class Registry {
private:
	std::string m_registryURL;

public:
	Registry(const std::string& registryURL) : m_registryURL(registryURL) {}

	void fetch_package(const std::string& packageName,
					   const std::string& version,
					   const std::filesystem::path& targetPath) const;

	void publish_package(const std::string& packageName,
						 const std::string& version,
						 const std::filesystem::path& sourcePath) const;
};
