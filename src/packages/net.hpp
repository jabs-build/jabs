#pragma once

#include <string>
#include <filesystem>

namespace net {

void fetch(const std::string& url, const std::filesystem::path& path,
		   bool fail_on_error = true);

void push(const std::string& url, const std::filesystem::path& path);

void push_string(const std::string& url, const std::string& data,
				 const std::string& content_type = "application/json");

} // namespace net
