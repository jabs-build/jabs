#include "source_collector.hpp"
#include "util/string.hpp"

#include <algorithm>

namespace fs = std::filesystem;

std::vector<fs::path>
SourceCollector::collect(const fs::path& directory) const {
	std::vector<fs::path> entries;
	auto dir_iterator = fs::recursive_directory_iterator(directory);
	for (const auto& p : dir_iterator) {
		auto filePath = p.path();
		if (p.is_directory()) {
			if (std::find(excluded_directories.begin(),
						  excluded_directories.end(),
						  filePath) != excluded_directories.end()) {
				dir_iterator
					.disable_recursion_pending(); // Don't descend into excluded
												  // directories
				continue;
			}
		} else {
			if (filePath.extension().empty())
				continue;
			if (std::find(excluded_files.begin(), excluded_files.end(),
						  filePath) != excluded_files.end())
				continue;
			for (const auto& ext : extensions) {
				if (filePath.extension() == ("." + ext)) {
					entries.push_back(filePath);
					break;
				}
			}
		}
	}
	return entries;
}

std::vector<fs::path>
SourceCollector::collect_all(const std::vector<fs::path>& directories) const {
	std::vector<fs::path> result;
	for (const auto& directory : directories) {
		auto collected = collect(directory);
		result.insert(result.end(), collected.begin(), collected.end());
	}
	for (const auto& manual : additional_files) {
		if (std::find(result.begin(), result.end(), manual) == result.end()) {
			result.push_back(manual);
		}
	}
	for (const auto& manual : additional_directories) {
		if (std::find_if(directories.begin(), directories.end(),
						 [&manual](const auto& directory) {
							 return string_starts_with(manual.string(),
												directory.string());
						 }) == directories.end()) {
			auto collected = collect(manual);
			result.insert(result.end(), collected.begin(), collected.end());
		}
	}
	return result;
}
