#pragma once

#include "build_profile.hpp"
#include "config.hpp"
#include "task.hpp"
#include "../compiler/driver.hpp"

#include <filesystem>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

std::unique_ptr<CompilerDriver> get_system_compiler();

class Module {
private:
	std::filesystem::path m_path;
	Config m_config;

public:
	static const std::string defaultSourceRoot;
	static const std::string defaultIncludeRoot;

	Module(const std::filesystem::path& path);
	Module(const std::filesystem::path& path, Config config);

	Task sync_dependencies(/* bool release_only */) const;
	Task build(BuildProfile profile) const;
	Task package_sources(BuildProfile profile = BuildProfile::Release) const;

	inline const Config& config() const noexcept { return m_config; }

	inline std::filesystem::path directory() const { return m_path; }
	inline std::filesystem::path target_directory() const { return m_path / "target"; }

	std::vector<std::filesystem::path> find_source_roots() const;
	std::vector<std::filesystem::path> find_include_roots() const;
	std::vector<std::filesystem::path> find_all_sources() const;
	std::vector<std::filesystem::path> find_compiled_sources() const;

	bool exists() const;

private:
	bool config_hash_exists() const;
	std::string read_config_hash() const;
	void write_config_hash(const std::string_view hash) const;
};
