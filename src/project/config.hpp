#pragma once

#include <map>
#include <string>
#include <vector>
#include <filesystem>

struct ObsoleteManual {
	std::vector<std::string> flags;
	std::vector<std::string> libraries;
	std::vector<std::filesystem::path> libraryDirectories;
	std::vector<std::filesystem::path> includeDirectories;
};

enum ProjectType { Library, Binary };

struct FilteredFileList {
	std::vector<std::string> only;
	std::vector<std::string> files;
};

struct Config {
	static const std::string defaultName;

	static Config load(const std::filesystem::path& path = ".");

	void save(const std::filesystem::path& path = ".") const;

	std::string name;
	std::string version;
	std::vector<std::string> authors;
	ProjectType type;
	std::string standard;
	std::vector<FilteredFileList> includes;
	std::vector<FilteredFileList> excludes;
	std::map<std::string, std::string> dependencies;
	std::string registry;
	ObsoleteManual obsolete;
};
