#include "task.hpp"

#include <atomic>
#include <iostream>
#include <thread>
#include <algorithm>
#include <memory>
#include <optional>

struct TaskExecutor {
	std::optional<std::thread> m_thread;
	std::atomic_bool& m_modified;
	std::vector<const Task*> m_tasks;

	TaskExecutor(std::atomic_bool& modified)
		: m_thread({}), m_modified(modified) {}

	void start() {
		m_thread = std::thread([this]() {
			for (const auto task : m_tasks) {
				if (task->run().changed())
					m_modified = true;
			}
		});
	}

	void join() {
		if (m_thread)
			m_thread->join();
	}
};

bool TaskList::run_all() const {
	bool modified = false;
	for (const auto& task : m_tasks) {
		if (task.run().changed())
			modified = true;
	}
	return modified;
}

bool TaskList::run_all_async(std::size_t max_threads) const {
	if (m_tasks.size() == 0) {
		return false;
	} else if (m_tasks.size() == 1) {
		return m_tasks[0].run().changed();
	} else {
		if (max_threads == 0) {
			max_threads = std::min(2u, std::thread::hardware_concurrency());
			if (max_threads == 0)
				max_threads = 2;
		}

		std::size_t nthreads = std::min(max_threads, m_tasks.size());
		std::cout << "running " << m_tasks.size() << " with " << nthreads
				  << " parallel executors" << std::endl;

		std::atomic_bool modified = false;

		std::vector<TaskExecutor> executors;
		executors.reserve(nthreads);
		for (std::size_t i = 0; i < nthreads; i++)
			executors.push_back(TaskExecutor(modified));

		for (std::size_t i = 0; i < m_tasks.size(); i++)
			executors[i % nthreads].m_tasks.push_back(&m_tasks[i]);

		for (auto& executor : executors)
			executor.start();

		for (auto& executor : executors)
			executor.join();

		return modified.load();
	}
}
