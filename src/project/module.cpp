#include "module.hpp"
#include "../compiler/gcc/driver.hpp" // TODO move with get_system_compiler
#include "../packages/registry.hpp"
#include "../process.hpp"
#include "../source_collector.hpp"

#include <fstream>
#include <iostream>
#include <mutex>

using namespace std::string_literals;

namespace fs = std::filesystem;

const std::string Module::defaultSourceRoot = "src";
const std::string Module::defaultIncludeRoot = "include";

Module::Module(const fs::path& path)
	: m_path(path), m_config(Config::load(path)) {}

Module::Module(const fs::path& path, Config config)
	: m_path(path), m_config(config) {}

bool Module::exists() const {
	return fs::exists(m_path / Config::defaultName);
}

std::string get_profile_directory_name(BuildProfile profile) {
	switch (profile) {
	case BuildProfile::Debug:
		return "debug";
	case BuildProfile::Release:
		return "release";
	default:
		std::cerr << "WARNING: Unknown profile "s +
						 std::to_string(static_cast<int>(profile))
				  << std::endl;
		return "unknown_"s + std::to_string(static_cast<int>(profile));
	}
}

std::unique_ptr<CompilerDriver> get_system_compiler() {
	// TODO replace this with kit detection
	return std::make_unique<GNUCompiler>();
}

void write_deps_file(const fs::path& path,
					 const std::vector<std::string>& deps) {
	std::ofstream depStream(path);
	if (!depStream)
		throw std::runtime_error("Failed to open dependency file");
	for (const auto& dep : deps)
		depStream << dep << '\n';
	depStream.flush();
	depStream.close();
}

std::vector<std::string> read_deps_file(const fs::path& path) {
	std::vector<std::string> result;
	std::ifstream input(path);
	if (!input)
		throw std::runtime_error("Failed to open dependency file for reading");
	std::string line;
	while (std::getline(input, line)) {
		if (!line.empty())
			result.push_back(line);
	}
	input.close();
	return result;
}

inline bool should_recompile(const fs::path& source, const fs::path& output,
							 const fs::path& depsFile) {
	if (!fs::exists(output) ||
		fs::last_write_time(source) > fs::last_write_time(output)) {
		return true;
	} else {
		auto dependencies = read_deps_file(depsFile);
		for (const auto& depName : dependencies) {
			fs::path dep(depName);
			if (!fs::exists(dep) ||
				fs::last_write_time(output) < fs::last_write_time(dep)) {
				return true;
			}
		}
		return false;
	}
}

const std::string system_flags[] = {
#if defined(unix) || defined(__unix) || defined(__unix__)
	"unix",
#endif
#if defined(__linux__) || defined(__linux) || defined(linux)
	"linux",
#endif
#if defined(_WIN32)
	"windows", "win32",
#endif
#if defined(_WIN64)
	"win64",
#endif
};

static bool is_active(const std::vector<std::string>& constraints) {
	for (const auto& constraint : constraints) {
		if (std::find(std::begin(system_flags), std::end(system_flags),
					  constraint) != std::end(system_flags)) {
			return true;
		}
	}
	return false;
}

template <typename T>
static void split_files_and_directories(const std::vector<T>& in,
									    std::vector<fs::path>& files,
									    std::vector<fs::path>& directories) {
	for (const auto& file : in) {
		if (fs::exists(file)) {
			if (fs::is_directory(file)) {
				directories.push_back(file);
			} else {
				files.push_back(file);
			}
		}
	}
}

std::vector<fs::path> Module::find_compiled_sources() const {
	SourceCollector collector;
	collector.extensions = {"cpp", "cxx", "cc"};

	for (const auto& filter : config().includes) {
		bool active = is_active(filter.only);
		if (active)
			split_files_and_directories(filter.files,
										collector.additional_files,
										collector.additional_directories);
		else
			split_files_and_directories(filter.files, collector.excluded_files,
										collector.excluded_directories);
	}

	for (const auto& filter : config().excludes) {
		bool active = is_active(filter.only);
		if (active)
			split_files_and_directories(filter.files, collector.excluded_files,
										collector.excluded_directories);
	}

	return collector.collect_all(find_source_roots());
}

std::vector<fs::path> Module::find_all_sources() const {
	SourceCollector collector;
	collector.extensions = {"cpp", "cxx", "cc", "hpp", "hxx", "hh", "h"};

	for (const auto& filter : config().includes) {
		bool active = is_active(filter.only);
		if (active)
			split_files_and_directories(filter.files,
										collector.additional_files,
										collector.additional_directories);
		else
			split_files_and_directories(filter.files, collector.excluded_files,
										collector.excluded_directories);
	}

	for (const auto& filter : config().excludes) {
		bool active = is_active(filter.only);
		if (active)
			split_files_and_directories(filter.files, collector.excluded_files,
										collector.excluded_directories);
	}

	std::vector<fs::path> roots;
	auto source_roots = find_source_roots();
	roots.insert(roots.end(), source_roots.begin(), source_roots.end());
	auto include_roots = find_include_roots();
	roots.insert(roots.end(), include_roots.begin(), include_roots.end());
	return collector.collect_all(roots);
}

Task Module::build(BuildProfile profile) const {
	return Task([this, profile]() {
		auto sources = find_compiled_sources();
		if (sources.empty()) {
			return false;
		}

		auto compiler = get_system_compiler();

		if (config().type == ProjectType::Binary)
			compiler->outputType = OutputType::Executable;
		else if (config().type == ProjectType::Library)
			compiler->outputType = OutputType::Static;

		compiler->buildProfile = profile;

		compiler->standard = config().standard;

		for (auto& includeDir : config().obsolete.includeDirectories)
			compiler->includeDirectories.push_back(includeDir);
		for (auto& libraryDir : config().obsolete.libraryDirectories)
			compiler->libraryDirectories.push_back(libraryDir);
		for (auto& lib : config().obsolete.libraries)
			compiler->libraries.push_back(lib);
		compiler->additionalFlags = config().obsolete.flags;

		auto include_roots = find_include_roots();
		compiler->includeDirectories.insert(compiler->includeDirectories.end(),
											include_roots.begin(),
											include_roots.end());

		std::string current_config_hash = compiler->get_config_hash();
		bool recompile_all =
			config_hash_exists() && read_config_hash() != current_config_hash;

		TaskList tasks;

		std::vector<fs::path> object_files;
		object_files.reserve(sources.size());
		std::mutex cout_mutex;

		fs::path targetPath =
			target_directory() / get_profile_directory_name(profile);
		fs::path binDirectory = targetPath / "intermediate";

		for (auto& source : sources) {
			fs::path objectDir =
				binDirectory / fs::relative(source.parent_path(), ".");
			if (!fs::exists(objectDir) && !fs::create_directories(objectDir))
				throw std::runtime_error("Failed to create directory "s +
										 objectDir.string());
			auto output = objectDir / source.filename().replace_extension(
										  source.extension().string() + ".o"s);
			object_files.push_back(output);

			auto depsFile =
				objectDir / source.filename().replace_extension(
								source.extension().string() + ".deps.txt"s);

			if (!recompile_all && !should_recompile(source, output, depsFile))
				continue;

			Task task([output, depsFile, recompile_all, &cout_mutex, &compiler,
					   &source]() {
				cout_mutex.lock();
				std::cout << "[ CC ] " << source.string() << " -> "
						  << output.string() << std::endl; // TODO verbose
				cout_mutex.unlock();

				CompileResult result = compiler->invoke_compile(source, output);

				write_deps_file(depsFile, result.includeDependencies);

				return TaskResult(true);
			});
			tasks.AddTask(task);
		}

		// TODO Balance threading
		bool any_object_touched = tasks.run_all_async();

		fs::path outputFile =
			targetPath / compiler->get_output_filename(config().name);

		if (recompile_all || any_object_touched || !fs::exists(outputFile)) {
			std::cout << "[LINK] " << outputFile.string()
					  << std::endl; // TODO verbose

			compiler->invoke_link(object_files, outputFile);

			write_config_hash(current_config_hash);
			return true;
		} else {
			return false;
		}
	});
}

Task Module::package_sources(BuildProfile profile) const {
	return Task([this, profile]() {
		fs::path output_file = target_directory() /
							   get_profile_directory_name(profile) /
							   "package.tar.gz";

		auto source_roots = find_source_roots();
		auto include_roots = find_include_roots();

		if (source_roots.empty() && include_roots.empty())
			throw std::runtime_error("No source directories found");

		auto config_file = directory() / Config::defaultName;

		std::vector<std::string> args{
			"czf", output_file.string(), "-C",
			fs::absolute(config_file).parent_path().string(),
			config_file.filename().string()};
		for (const auto& source_root : source_roots) {
			args.push_back("-C");
			args.push_back(fs::absolute(source_root).parent_path().string());
			args.push_back(source_root.filename().string());
		}
		for (const auto& include_root : include_roots) {
			args.push_back("-C");
			args.push_back(fs::absolute(include_root).parent_path().string());
			args.push_back(include_root.filename().string());
		}

		std::cout << "[PCKG] " << output_file.string() << std::endl;
		// TODO linux only
		if (run_process("tar", args) != 0)
			throw std::runtime_error("Failed to run tar.");

		return true;
	});
}

Task Module::sync_dependencies() const {
	return Task([this]() {
		Registry registry(config().registry);
		const auto packages_directory = target_directory() / "packages";
		fs::create_directory(packages_directory);
		for (const auto& [name, version] : config().dependencies) {
			registry.fetch_package(name, version,
								   packages_directory / (name + ".tar.gz"s));
		}
		return false;
	});
}

bool Module::config_hash_exists() const {
	return fs::exists(target_directory() / "confighash.txt");
}

std::string Module::read_config_hash() const {
	std::ifstream in(target_directory() / "confighash.txt");
	if (!in.is_open())
		throw std::runtime_error("Failed to read config hash");
	std::string hash;
	getline(in, hash);
	in.close();
	return hash;
}

void Module::write_config_hash(const std::string_view hash) const {
	std::ofstream out(target_directory() / "confighash.txt");
	if (!out.is_open())
		throw std::runtime_error("Failed to write config hash");
	out << hash << std::endl;
	out.close();
}

std::vector<fs::path> Module::find_source_roots() const {
	std::vector<fs::path> source_roots;
	if (fs::exists(Module::defaultSourceRoot))
		source_roots.push_back(Module::defaultSourceRoot);
	return source_roots;
}

std::vector<fs::path> Module::find_include_roots() const {
	std::vector<fs::path> include_roots;
	if (fs::exists(Module::defaultIncludeRoot))
		include_roots.push_back(Module::defaultIncludeRoot);
	return include_roots;
}
