#include "config.hpp"

#include <fstream>
#include <stdexcept>

#include <yaml-cpp/yaml.h>

#include <cassert>

using namespace std::string_literals;

namespace fs = std::filesystem;

const std::string Config::defaultName = "jabs.yml";

static void
parse_filtered_file_list(const YAML::Node& node,
					  std::vector<FilteredFileList>& output_list) {
	if (node.IsSequence()) {
		for (const auto& t : node) {
			if (t.IsMap()) {
				FilteredFileList list;
				auto only = t["only"];
				if (only.IsDefined()) {
					if (only.IsSequence())
						list.only = only.as<std::vector<std::string>>();
					else
						list.only.push_back(only.as<std::string>());
				}
				auto files = t["files"];
				if (files.IsSequence())
					list.files = files.as<std::vector<std::string>>();
				else
					list.files.push_back(files.as<std::string>());

				output_list.push_back(list);
			} else {
				auto file = t.as<std::string>();
				FilteredFileList list;
				list.files.push_back(file);
				output_list.push_back(list);
			}
		}
	} else {
		auto inc = node.as<std::string>();
		FilteredFileList list;
		list.files.push_back(inc);
		output_list.push_back(list);
	}
}

Config Config::load(const fs::path& path) {
	fs::path filePath = fs::is_directory(path) ? path / defaultName : path;
	if (!fs::exists(filePath))
		throw std::runtime_error("Config file doesn't exist");

	YAML::Node root = YAML::LoadFile(filePath);

	Config config;
	config.name = root["name"].as<std::string>();

	config.version = root["version"].as<std::string>();

	auto auth = root["authors"];
	if (auth.IsDefined()) {
		if (auth.IsSequence()) {
			config.authors = auth.as<std::vector<std::string>>();
		} else {
			config.authors.push_back(auth.as<std::string>());
		}
	}

	auto type = root["type"];
	if (type.IsDefined()) {
		auto typeString = type.as<std::string>();
		if (typeString == "binary")
			config.type = ProjectType::Binary;
		else if (typeString == "library")
			config.type = ProjectType::Library;
		else
			throw std::runtime_error("Invalid project type "s + typeString);
	} else {
		config.type = ProjectType::Binary;
	}

	auto standard = root["standard"];
	if (standard.IsDefined()) {
		config.standard = standard.as<std::string>();
		// TODO ansi?
		if (config.standard != "c++98" && config.standard != "c++03" &&
			config.standard != "c++11" && config.standard != "c++14" &&
			config.standard != "c++17" && config.standard != "c++2a") {
			throw std::runtime_error("Invalid c++ standard "s +
									 config.standard);
		}
	} else {
		config.standard = "c++17";
	}

	auto registry = root["registry"];
	if (registry.IsDefined()) {
		config.registry = registry.as<std::string>();
	}

	auto dependencies = root["dependencies"];
	if (dependencies.IsDefined()) {
		config.dependencies =
			dependencies.as<std::map<std::string, std::string>>();
	}

	auto includes = root["include"];
	if (includes.IsDefined()) {
		parse_filtered_file_list(includes, config.includes);
	}

	auto excludes = root["exclude"];
	if (excludes.IsDefined()) {
		parse_filtered_file_list(excludes, config.excludes);
	}

	auto obs = root["_obsolete"];
	if (obs.IsDefined()) {
		const auto lib_names = obs["lib_names"];
		if (lib_names.IsDefined())
			config.obsolete.libraries = lib_names.as<std::vector<std::string>>();
		const auto lib_paths = obs["lib_paths"];
		if (lib_paths.IsDefined()) {
			const auto lib_path_vec = lib_paths.as<std::vector<std::string>>();
			config.obsolete.libraryDirectories.insert(
				config.obsolete.libraryDirectories.end(), lib_path_vec.begin(),
				lib_path_vec.end());
		}
		const auto include_paths = obs["include_paths"];
		if (include_paths.IsDefined()) {
			const auto include_path_vec = include_paths.as<std::vector<std::string>>();
			config.obsolete.includeDirectories.insert(
				config.obsolete.includeDirectories.end(), include_path_vec.begin(),
				include_path_vec.end());
		}
		const auto flags = obs["flags"];
		if (flags.IsDefined()) {
			if (flags.IsSequence())
				config.obsolete.flags = flags.as<std::vector<std::string>>();
			else
				config.obsolete.flags.push_back(flags.as<std::string>());
		}
	}

	return config;
}

void Config::save(const fs::path& path) const {
	fs::path filePath = fs::is_directory(path) ? path / defaultName : path;

	YAML::Node root;

	root["name"] = name;

	root["version"] = version;

	if (authors.size() > 1) {
		root["authors"] = authors;
	} else if (authors.size() == 1) {
		root["authors"] = authors[0];
	}

	if (!registry.empty()) {
		root["registry"] = registry;
	}

	if (!dependencies.empty()) {
		root["dependencies"] = dependencies;
	}

	if (type == ProjectType::Binary)
		root["type"] = "binary";
	else if (type == ProjectType::Library)
		root["type"] = "library";
	else
		assert(false);

	if (!standard.empty())
		root["standard"] = standard;

	if (!includes.empty()) {
		for (const auto& inc : includes) {
			if (inc.files.empty())
				continue;

			YAML::Node include_node;

			if (inc.only.size() == 1)
				include_node["only"] = inc.only[0];
			else if (inc.only.size() > 1)
				include_node["only"] = inc.only;

			if (inc.files.size() == 1)
				include_node["files"] = inc.files[0];
			else
				include_node["files"] = inc.files;

			root["include"].push_back(include_node);
		}
	}

	if (!excludes.empty()) {
		for (const auto& exc : excludes) {
			if (exc.files.empty())
				continue;

			YAML::Node exclude_node;

			if (exc.only.size() == 1)
				exclude_node["only"] = exc.only[0];
			else if (exc.only.size() > 1)
				exclude_node["only"] = exc.only;

			if (exc.files.size() == 1)
				exclude_node["files"] = exc.files[0];
			else
				exclude_node["files"] = exc.files;

			root["exclude"].push_back(exclude_node);
		}
	}

	std::ofstream output(filePath);
	output << root;
	output << std::endl; // yaml node doesn't have a newline when serialized
}
