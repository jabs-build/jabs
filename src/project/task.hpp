#pragma once

#include <functional>
#include <vector>
#include <cstddef>

class TaskResult {
private:
	bool m_changed;

public:
	TaskResult(bool changed) : m_changed(changed) {}

	inline bool changed() const { return m_changed; }
};

class Task {
public:
	using TaskFunc = std::function<TaskResult()>;

private:
	TaskFunc m_task_callback;

public:
	Task(TaskFunc task_callback) : m_task_callback(task_callback) {}

	inline TaskResult run() const { return m_task_callback(); }
};

class TaskList {
private:
	std::vector<Task> m_tasks;

public:
	inline void AddTask(Task task) { m_tasks.push_back(task); };

	bool run_all() const;
	// max_threads 0 => As many threads as available
	bool run_all_async(std::size_t max_threads = 0) const;
};
