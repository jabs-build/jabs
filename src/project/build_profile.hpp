#pragma once

#include <string>

enum class BuildProfile { Debug, Release };

std::string get_profile_directory_name(BuildProfile profile);
