#include "project.hpp"

#include "config.hpp"

#include <csignal>
#define debugger std::raise(SIGTRAP)

namespace fs = std::filesystem;

Project::Project(const fs::path& path) {
	auto config_path = path.filename().string() != "jabs.yml"
		? (path / "jabs.yml")
		: path;
	Config root_config = Config::load(path);
	// TODO workspace type projects
	auto root_module = std::make_shared<Module>(path, root_config);
	m_modules.push_back(root_module);
	m_root_module = root_module;
	
	resolve_dependencies();
}

#include <iostream>

void Project::resolve_dependencies() {
	std::vector<Module> new_modules;
	for (const auto& module : m_modules) {
		for (const auto& dependency : module->config().dependencies) {
			const auto& spec = std::get<1>(dependency);
			if (spec.length() == 0) continue;
			if (spec.at(0) == '.') {
				// Dependency is local path
				auto module_path = fs::path(spec);
				auto loaded_module = std::find_if(m_modules.begin(), m_modules.end(),
					[&module_path](const auto& mod){ return mod->directory() == module_path; });
				if (loaded_module == m_modules.end()) {
					// TODO Module() throws?
					new_modules.push_back(Module(module_path));
				}
			}
		}
	}
	for (auto module : new_modules) {
		m_modules.push_back(std::make_shared<Module>(module));
	}
}
