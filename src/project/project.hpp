#pragma once

#include <optional>
#include <vector>
#include <filesystem>
#include <memory>

#include "module.hpp"

class Project {
private:
	std::optional<std::weak_ptr<Module>> m_root_module;
	std::vector<std::shared_ptr<Module>> m_modules;
public:
	explicit Project(const std::filesystem::path& path);

	void resolve_dependencies();

	inline bool has_root_module() const {
		return m_root_module.has_value();
	}

	inline std::shared_ptr<Module> root_module() const {
		if (!m_root_module)
			return {};
		return m_root_module->lock();
	}
};
