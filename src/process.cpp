#include "process.hpp"

#include <cstring>
#include <stdexcept>
#include <thread>

using namespace std::string_literals;

#if defined(__linux)

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <array>

int run_process(const std::string& file, const std::vector<std::string>& args,
			   bool capture_output) {
	int sout[2], serr[2];

	if (capture_output) {
		// Create stdout/stderr pipes
		if (pipe(sout) != 0)
			throw std::runtime_error("Failed to create stdout pipe: "s +
									 std::string(strerror(errno)));
		if (pipe(serr) != 0)
			throw std::runtime_error("Failed to create stderr pipe: "s +
									 std::string(strerror(errno)));
	}

	pid_t forkedPID = fork();
	if (forkedPID == 0) { // Child branch
		if (capture_output) {
			// Close stdout/stderr write ends (unused in child)
			close(sout[0]);
			close(serr[0]);

			// Redirect stdout/stderr
			if (dup2(sout[1], STDOUT_FILENO) < 0) {
				perror("jabs: Failed to redirect stdout");
				exit(1);
			}
			if (dup2(serr[1], STDOUT_FILENO) < 0) {
				perror("jabs: Failed to redirect stderr");
				exit(1);
			}
		}

		std::vector<char*> argv;
		argv.reserve(args.size() + 1);
		// Assume (hope) that execvp won't modify the arguments array
		argv.push_back(const_cast<char*>(file.c_str()));
		for (auto& arg : args)
			argv.push_back(const_cast<char*>(arg.c_str()));
		argv.push_back(nullptr);
		execvp(file.c_str(), argv.data());
		exit(1);
		return 1;
	} else if (forkedPID > 0) { // Parent branch
		if (capture_output) {
			// Close stdout/stderr read ends (unused in parent)
			close(sout[1]);
			close(serr[1]);

			std::string soutOutput;
			std::thread soutThread([sout, &soutOutput] {
				std::array<char, 1024> buffer;
				while (true) {
					ssize_t readSize =
						read(sout[0], buffer.data(), buffer.size());
					if (readSize < 0)
						throw std::runtime_error("stdout read failed: "s +
												 std::string(strerror(errno)));
					else if (readSize == 0)
						break;
					else
						soutOutput.append(std::string(buffer.data(), readSize));
				}
				close(sout[0]);
			});

			std::string serrOutput;
			std::thread serrThread([serr, &serrOutput] {
				std::array<char, 1024> buffer;
				while (true) {
					ssize_t readSize =
						read(serr[0], buffer.data(), buffer.size());
					if (readSize < 0)
						throw std::runtime_error("stdout read failed: "s +
												 std::string(strerror(errno)));
					else if (readSize == 0)
						break;
					else
						serrOutput.append(std::string(buffer.data(), readSize));
				}
				close(serr[0]);
			});

			soutThread.join();
			serrThread.join();
		}

		int status;
		if (waitpid(forkedPID, &status, 0) < 0)
			throw std::runtime_error("Wait failed: "s +
									 std::string(strerror(errno)));

		return WEXITSTATUS(status);
	} else {
		if (capture_output) {
			close(sout[0]);
			close(sout[1]);
			close(serr[0]);
			close(serr[1]);
		}
		throw std::runtime_error("Fork failed: "s +
								 std::string(strerror(errno)));
	}
}

#elif defined(_WIN32)

#include <Windows.h>
#include <cstring>
#include <sstream>
#include <string_view>

namespace {
	std::unique_ptr<char[]> make_mutable_c_str(const std::string_view input) {
		auto mutable_str = std::make_unique<char[]>(input.size() + 1);
		std::strncpy(mutable_str.get(), input.data(), input.size());
		mutable_str[input.size()] = '\0';
		return mutable_str;
	}
}

int run_process(const std::string& file, const std::vector<std::string>& args,
			    bool capture_output) {
	std::stringstream ss;
	for (int i = 0; i < args.size(); i++) {
		ss << args[i];
		if (i != args.size() - 1)
			ss << " ";
	}
	auto c_args = make_mutable_c_str(ss.str());

	STARTUPINFOA info = {sizeof(info)};
	PROCESS_INFORMATION processInfo;
	if (!CreateProcessA(file.c_str(), c_args.get(), NULL, NULL, true, 0, NULL,
						NULL, &info, &processInfo))
		throw std::runtime_error("Failed to start process: "s +
								 std::to_string(GetLastError()));

	WaitForSingleObject(processInfo.hProcess, INFINITE);
	DWORD exitCode;
	if (!GetExitCodeProcess(processInfo.hProcess, &exitCode))
		throw std::runtime_error("Could not get process exit code: "s +
								 std::to_string(GetLastError()));
	CloseHandle(processInfo.hProcess);
	CloseHandle(processInfo.hThread);
	return exitCode;
}

#endif
