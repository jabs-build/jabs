#include "named_pipe.hpp"

#include <sys/stat.h>

#include <stdexcept>
#include <string>
using namespace std::string_literals;
#include <cerrno>
#include <cstring>

#include <iostream>

namespace fs = std::filesystem;

NamedPipe::NamedPipe(const fs::path& path, bool removeIfPresent)
	: m_path(path) {
	if (removeIfPresent && fs::exists(path)) {
		fs::remove(path);
	}
	if (mkfifo(path.c_str(), S_IRUSR | S_IWUSR) != 0) {
		throw std::runtime_error("Failed to open named pipe "s + path.string() +
								 ": "s + std::string(std::strerror(errno)));
	}
}

NamedPipe::~NamedPipe() {}

void NamedPipe::close() {
	if (!fs::remove(m_path)) {
		std::cerr << "Named pipe " << m_path.string() << " didn't exist on close";
	}
}
