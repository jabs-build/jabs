#pragma once

#ifndef __linux__
#error "Named pipes only available on Linux"
#endif

#include <filesystem>

class NamedPipe {
private:
	std::filesystem::path m_path;

public:
	NamedPipe(const std::filesystem::path& path, bool removeIfPresent = false);
	~NamedPipe();
	void close();
};
