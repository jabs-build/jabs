#pragma once

#include <cstddef>
#include <string>
#include <iterator>

namespace encoding {

namespace base64 {

constexpr const char* alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
								 "abcdefghijklmnopqrstuvwxyz"
								 "0123456789+/";

template <typename Iter> std::string encode(Iter begin, Iter end) {
	std::string ret;
	char char_array_3[3];
	char char_array_4[4];

	int i = 0;
	while (begin != end) {
		char_array_3[i++] = *begin;
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) +
							  ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) +
							  ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; i < 4; i++)
				ret += alphabet[static_cast<size_t>(char_array_4[i])];
			i = 0;
		}
		std::advance(begin, 1);
	}

	if (i) {
		for (int j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] =
			((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] =
			((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (int j = 0; (j < i + 1); j++)
			ret += alphabet[static_cast<size_t>(char_array_4[j])];

		while ((i++ < 3))
			ret += '=';
	}

	return ret;
}
} // namespace base64

namespace hex {

constexpr const char* alphabet = "0123456789ABCDEF";

template <typename I>
std::string encode_int(I w, size_t hex_len = sizeof(I) << 1) {
	std::string result(hex_len, '0');
	for (size_t i = 0, j = (hex_len - 1) * 4; i < hex_len; ++i, j -= 4)
		result[i] = alphabet[(w >> j) & 0xF];
	return result;
}

} // namespace hex

} // namespace encoding
