#include "file.hpp"


#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <string>
using namespace std::string_literals;

PosixFileDescriptor::PosixFileDescriptor(const std::filesystem::path& path, int flags)
	: m_file(::open(path.c_str(), flags)) {
	if (m_file < 0)
		throw std::runtime_error("Failed to open file: "s + std::strerror(errno));
}

PosixFileDescriptor::~PosixFileDescriptor() {
	::close(m_file);
}

CFile::CFile(const std::filesystem::path& path, std::string_view mode)
	: m_file(std::fopen(path.c_str(), std::string(mode).c_str())) {
	if (!m_file)
		throw std::runtime_error("Could not open file");
}

CFile::~CFile() {
	std::fclose(m_file);
}
