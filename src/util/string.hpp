#pragma once

#include <string>
#include <algorithm>

// TODO replace these with std methods once c++20 is supported

bool string_starts_with(const std::string& value, const std::string& start) {
	if (start.size() > value.size())
		return false;
	return std::equal(start.begin(), start.end(), value.begin());
}

bool string_ends_with(const std::string& value, const std::string& end) {
	if (end.size() > value.size())
		return false;
	return std::equal(end.rbegin(), end.rend(), value.rbegin());
}
