#pragma once

#include <string_view>
#include <filesystem>
#include <cstdio>

class PosixFileDescriptor {
private:
	int m_file;

public:
	PosixFileDescriptor(const std::filesystem::path& path, int flags);
	~PosixFileDescriptor();

	inline int fd() const { return m_file; }
};

class CFile {
private:
	FILE* m_file;

public:
	CFile(const std::filesystem::path& path, std::string_view mode);
	~CFile();

	inline const FILE* file() const { return m_file; }
};
