#include "argument-parser.hpp"

#include <algorithm>
#include <cstddef>

Arguments ArgumentParser::parse(std::vector<std::string_view> args) {
	Arguments result;
	for (size_t i = 0; i < args.size(); i++) {
		auto arg = args[i];
		if (arg.length() >= 2 && arg[0] == '-' && arg[1] == '-') {
			if (arg.length() == 2) {
				i++;
				for (; i < args.size(); i++) {
					result.restArguments.push_back(args[i]);
				}
				break;
			}
			arg = arg.substr(2);
		} else if (arg.length() > 1 && arg[0] == '-') {
			arg = arg.substr(1);
		} else {
			result.positionalArguments.push_back(arg);
			continue;
		}
		auto equals = arg.find('=');
		if (equals != std::string_view::npos) {
			auto key = arg.substr(0, equals);
			auto value = arg.substr(equals);
			result.arguments.emplace(key, value);
		} else {
			result.flags.push_back(arg);
		}
	}
	return result;
}
