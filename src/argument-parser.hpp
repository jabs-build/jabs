#pragma once

#include <map>
#include <string_view>
#include <vector>

struct Arguments {
	std::vector<std::string_view> flags;
	std::map<std::string_view, std::string_view> arguments;
	std::vector<std::string_view> positionalArguments;
	/** Arguments after `--` */
	std::vector<std::string_view> restArguments;
};

class ArgumentParser {
public:
	Arguments parse(std::vector<std::string_view> args);
};
