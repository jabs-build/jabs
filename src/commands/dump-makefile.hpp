#pragma once

#include "command.hpp"

struct Arguments;
class Project;

class DumpMakefileCommand : public Command {
public:
	inline DumpMakefileCommand() : Command("dump-makefile", true) {}

	void invoke(const Arguments& args, Project* project) const override;
};
