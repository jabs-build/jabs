#pragma once

#include "command.hpp"

class Project;
struct Arguments;

class BuildCommand : public Command {
public:
	inline BuildCommand() : Command("build", true) {}

	void invoke(const Arguments& args, Project* project) const override;
};
