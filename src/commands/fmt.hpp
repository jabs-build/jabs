#pragma once

#include "command.hpp"

class Project;
struct Arguments;

class FormatCommand : public Command {
public:
	inline FormatCommand() : Command("fmt", true) {}

	void invoke(const Arguments& args, Project* project) const override;
};
