#include "run.hpp"
#include "../process.hpp"

#include <iostream>
#include "../argument-parser.hpp"
#include "../project/module.hpp"
#include "../project/project.hpp"

using namespace std::string_literals;

void RunCommand::invoke(const Arguments& args, Project* project) const {
	auto module = project->root_module();

	if (module->config().type != ProjectType::Binary) {
		std::cerr << "Library projects can't be executed" << std::endl;
		return;
	}

	BuildProfile profile = BuildProfile::Debug;
	if (std::find(args.flags.begin(), args.flags.end(), "release") !=
		args.flags.end()) {
		profile = BuildProfile::Release;
		std::cout << "Compiling for release" << std::endl;
	}

	module->build(profile).run();

	auto executable =
		module->target_directory() / get_profile_directory_name(profile) /
		get_system_compiler()->get_output_filename(module->config().name);

	std::vector<std::string> projectArgs(args.restArguments.begin(), args.restArguments.end());
	int code = run_process(executable, projectArgs);
	if (code != 0)
		throw std::runtime_error("Process exited with code "s +
								 std::to_string(code));
}
