#include "command.hpp"

#include "dump-makefile.hpp"
#include "build.hpp"
#include "publish.hpp"
#include "init.hpp"
#include "run.hpp"

std::vector<std::shared_ptr<Command>> Command::commands = {
	std::make_shared<DumpMakefileCommand>(),
	std::make_shared<BuildCommand>(),
	std::make_shared<PublishCommand>(),
	std::make_shared<InitCommand>(),
	std::make_shared<RunCommand>()
};

const std::shared_ptr<Command> Command::get_by_alias(std::string_view alias) {
	for (const auto& command : commands)
		if (command->alias() == alias)
			return command;
	return nullptr;
}

const std::vector<std::shared_ptr<Command>>& Command::get_all() {
	return commands;
}
