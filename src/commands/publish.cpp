#include "publish.hpp"

#include "../packages/registry.hpp"
#include "../project/module.hpp"
#include "../project/project.hpp"

#include <iostream>
#include <filesystem>

void PublishCommand::invoke(const Arguments&, Project* project) const {
	auto module = project->root_module();
	auto config = module->config();

	if (config.registry.empty()) {
		std::cerr << "No registry configured." << std::endl;
		return;
	}

	std::cout << "Validating that package compiles ..." << std::endl;
	module->build(BuildProfile::Release).run();

	std::cout << "Packaging sources ..." << std::endl;
	module->package_sources().run();

	std::filesystem::path tarPath = module->target_directory() / "release/package.tar.gz";

	Registry registry(config.registry);
	registry.publish_package(config.name, config.version, tarPath);
}
