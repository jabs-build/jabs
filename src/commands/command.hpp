#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <vector>

class Project;
struct Arguments;

class Command {
private:
	static std::vector<std::shared_ptr<Command>> commands;

public:
	static const std::shared_ptr<Command>
	get_by_alias(std::string_view alias);

	static const std::vector<std::shared_ptr<Command>>& get_all();

private:
	std::string m_alias;
	bool m_requires_project;

protected:
	inline Command(const std::string& alias, bool requires_project = false)
		: m_alias(alias), m_requires_project(requires_project) {}

	virtual ~Command() = default;

public:
	virtual void invoke(const Arguments& args, Project* project) const = 0;

	inline const std::string& alias() const { return m_alias; };
	inline bool requires_project() const { return m_requires_project; }
};
