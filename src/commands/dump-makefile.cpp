#include "dump-makefile.hpp"

#include "../project/module.hpp"
#include "../project/project.hpp"
#include "../source_collector.hpp"
#include "../argument-parser.hpp"

#include <string>
using namespace std::string_literals;

#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <utility>
#include <vector>

namespace fs = std::filesystem;

void DumpMakefileCommand::invoke(const Arguments& args,
								 Project* project) const {
	auto module = project->root_module();
	if (args.positionalArguments.size() <= 1) {
		std::cerr << "ERROR: dump-makefile requires at least one argument"
				  << std::endl;
		std::cerr << "Usage: dump-makefile <makefile>" << std::endl;
		return;
	}

	BuildProfile profile = BuildProfile::Debug;
	if (std::find(args.flags.begin(), args.flags.end(), "release") !=
		args.flags.end())
		profile = BuildProfile::Release;

	fs::path makefile(args.positionalArguments[1]);

	std::cout << "Generating Makefile" << std::endl;

	auto source_roots = module->find_source_roots();
	auto include_roots = module->find_include_roots();

	auto compiler = get_system_compiler();
	if (source_roots.empty() &&
		include_roots.empty()) // TODO include-only projects
		throw std::runtime_error("No source directories found");

	fs::path targetPath = fs::path("temp");
	fs::path binDirectory = targetPath / "intermediate";

	SourceCollector collector;
	collector.extensions.push_back("cpp");
	collector.extensions.push_back("cxx");
	collector.extensions.push_back("cc");
	auto sources = collector.collect_all(source_roots);

	compiler->outputType = OutputType::Executable;
	compiler->buildProfile = profile;

	for (auto& includeDir : module->config().obsolete.includeDirectories)
		compiler->includeDirectories.push_back(includeDir);
	for (auto& libraryDir : module->config().obsolete.libraryDirectories)
		compiler->libraryDirectories.push_back(libraryDir);
	for (auto& lib : module->config().obsolete.libraries)
		compiler->libraries.push_back(lib);

	compiler->includeDirectories.insert(compiler->includeDirectories.end(),
										include_roots.begin(),
										include_roots.end());

	std::vector<fs::path> object_files;
	object_files.reserve(sources.size());

	std::ofstream outstr(makefile);
	if (!outstr.is_open())
		throw std::runtime_error("Failed to open output stream");

	for (auto& source : sources) {
		fs::path objectDir =
			binDirectory / fs::relative(source.parent_path(), ".");
		auto output = objectDir / source.filename().replace_extension(
									  source.extension().string() + ".o"s);
		object_files.push_back(output);
	}

	fs::path output_file =
		targetPath / compiler->get_output_filename(module->config().name);

	outstr << "all: " << output_file.string() << '\n';
	outstr << ".PHONY: all\n\n";

	outstr << output_file.string() << ":";
	for (const auto& obj : object_files)
		outstr << ' ' << obj.string();
	outstr << '\n';
	auto [link_executable, link_args] =
		compiler->build_link_command_line(object_files, output_file);
	outstr << '\t' << link_executable;
	for (const auto& arg : link_args)
		outstr << ' ' << arg;
	outstr << "\n\n";

	for (std::size_t i = 0; i < sources.size(); i++) {
		const auto& source = sources[i];
		const auto& output = object_files[i];

		auto [executable, args] =
			compiler->build_compile_command_line(source, output);
		outstr << output.string() << ": " << source.string() << "\n";
		outstr << "\t@mkdir -p \"" << output.parent_path().string() << "\"\n";
		outstr << '\t' << executable;
		for (const auto& arg : args)
			outstr << ' ' << arg;
		outstr << "\n\n";
	}
	outstr.flush();
	outstr.close();
}
