#include "build.hpp"

#include <iostream>
#include <algorithm>
#include <vector>

#include "../project/module.hpp"
#include "../project/project.hpp"
#include "../project/build_profile.hpp"
#include "../project/task.hpp"
#include "../argument-parser.hpp"

void BuildCommand::invoke(const Arguments& args, Project* project) const {
	BuildProfile profile = BuildProfile::Debug;
	if (std::find(args.flags.begin(), args.flags.end(), "release") !=
		args.flags.end())
		profile = BuildProfile::Release;

	// project.sync_dependencies().run();

	if (profile == BuildProfile::Release) {
		std::cout << "Compiling for release" << std::endl;
	}

	bool did_compile_something = project->root_module()->build(profile).run().changed();
	if (!did_compile_something) {
		std::cout << "Nothing to do" << std::endl;
	}
}
