#include "fmt.hpp"

#include <filesystem>
#include <string>
#include <vector>

#include "../process.hpp"
#include "../project/module.hpp"
#include "../project/project.hpp"

void FormatCommand::invoke(const Arguments&, Project* p) const {
	auto sources = p->root_module()->find_all_sources();
	std::vector<std::string> args;
	args.push_back("-i");
	for (const auto& source : sources)
		args.push_back(source.string());
	run_process("clang-format", args, false);
}
