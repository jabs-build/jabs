#pragma once

#include "command.hpp"

class Project;
struct Arguments;

class InitCommand : public Command {
public:
	inline InitCommand() : Command("init", false) {}

	void invoke(const Arguments& args, Project* project) const override;
};
