#pragma once

#include "command.hpp"

struct Arguments;
class Project;

class PublishCommand : public Command {
public:
	inline PublishCommand() : Command("publish", true) {}

	void invoke(const Arguments& args, Project* project) const override;
};
