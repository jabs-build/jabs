#include "init.hpp"


#include <fstream>
#include <iostream>

#include "../argument-parser.hpp"
#include "../project/module.hpp"
#include "../project/config.hpp"

namespace fs = std::filesystem;

void InitCommand::invoke(const Arguments& args, Project*) const {
	auto project_directory = fs::current_path();

	if (fs::exists(project_directory / Config::defaultName))
		throw std::runtime_error("Project already exists in current directory");

	std::cout << "Creating new project in " << fs::current_path() << "..."
			  << std::endl;

	Config config;

	if (project_directory.has_filename()) {
		std::cout << "Name (" << project_directory.filename().string()
				  << "): " << std::flush;
		std::getline(std::cin, config.name);
		if (config.name.empty())
			config.name = project_directory.filename().string();
	} else {
		std::cout << "Name: " << std::flush;
		std::getline(std::cin, config.name);
	}

	std::cout << "Author: " << std::flush;
	std::string author;
	if (std::getline(std::cin, author) && !author.empty())
		config.authors.push_back(author);

	config.version = "0.0.1";

	if (std::find(args.flags.begin(), args.flags.end(), "lib") !=
		args.flags.end())
		config.type = ProjectType::Library;
	else
		config.type = ProjectType::Binary;

	config.save(project_directory);

	fs::create_directory(project_directory / "src");

	{
		std::ofstream gitignore(project_directory / ".gitignore");
		gitignore << "target/" << std::endl;
	}

	if (config.type == ProjectType::Binary) {
		std::ofstream maincpp(project_directory / "src" / "main.cpp");
		maincpp << "#include <iostream>" << std::endl;
		maincpp << std::endl;
		maincpp << "int main() {" << std::endl;
		maincpp << "\tstd::cout << \"Hello World!\" << std::endl;" << std::endl;
		maincpp << "}" << std::endl;
	}
}
