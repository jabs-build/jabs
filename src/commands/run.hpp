#pragma once

#include "command.hpp"

class RunCommand : public Command {
public:
	inline RunCommand() : Command("run", true) {}

	void invoke(const Arguments& args, Project* project) const override;
};
