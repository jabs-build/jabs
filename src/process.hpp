#pragma once

#include <string>
#include <vector>

int run_process(const std::string& file, const std::vector<std::string>& args,
			    bool capture_output = false);
