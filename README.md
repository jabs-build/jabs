# JABS

Just Another Build System that tries to follow an approach similar to [cargo](https://doc.rust-lang.org/cargo/).

## Creating a new project

To create a new project, just run this command:
```sh
jabs init
```

This will ask you a few questions, and then create a new (binary) project in the current directory, with a ``jabs.yml`` config that looks like this:

```yml
name: hello-world
version: 0.0.1
type: binary
```

To instead create a library project, add ``--lib`` to the command line.

## Building and running

```sh
jabs build

# This will only work for a binary project
jabs run
```

## What makes JABS different (and why you might or might _not_ want to use it)

In contrast to build systems like [CMake](https://cmake.org/), it does not generate files for another build system like make or ninja,
but instead avoids this level of indirection and directly invokes the compiler.

Also, JABS does not use a full blown configuration language, but relies on [yaml](https://yaml.org/).
On the one hand, this can simplify configuration a lot, but on the other hand, a full language can provide much more flexibility.

Also, disclaimer: This project is in a very early stage of development, so important features are missing and several bugs might exist.