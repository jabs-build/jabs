OUTPUT=temp/jabs

CXX=g++
CXXFLAGS=-std=c++17 -g -Og -Itemp/deps/include -Wall -Wextra
LDFLAGS=-std=c++17 -g -Og -Ltemp/deps/lib -lyaml-cpp -lstdc++fs -lcurl -lpthread

RM ?= rm -f

SOURCES := $(shell find src/ -type f -regex '.*\.cpp')
OBJECTS := $(patsubst src/%.cpp,temp/intermediate/%.cpp.o,$(SOURCES))

all: $(OUTPUT)
.PHONY: all

clean:
	$(RM) -r temp/intermediate
	$(RM) temp/jabs
.PHONY: clean

temp/intermediate/%.cpp.o: src/%.cpp
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(OUTPUT): $(OBJECTS)
	@mkdir -p $(dir $@)
	$(CXX) -o $@ $^ $(LDFLAGS)


